package main

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
)

var (
	port        string
	frontendURL string
	backendURL  string
	apiPath     string
	imagePath   string
)

func config() {
	port = os.Getenv("PORT")
	frontendURL = os.Getenv("FRONTEND_URL")
	backendURL = os.Getenv("BACKEND_URL")
	apiPath = os.Getenv("API_PATH")
	imagePath = os.Getenv("IMAGE_PATH")
}

func printConfig() {
	fmt.Println("port:", port)
	fmt.Println("frontend:", frontendURL)
	fmt.Println("backend:", backendURL)
	fmt.Println("api path:", apiPath)
	fmt.Println("image path:", imagePath)
}

func serveReverseProxy(target string, w http.ResponseWriter, r *http.Request) {
	url, _ := url.Parse(target)
	proxy := httputil.NewSingleHostReverseProxy(url)
	r.URL.Host = url.Host
	r.URL.Scheme = url.Scheme
	r.Header.Set("X-Forwarded-Host", r.Header.Get("Host"))
	r.Host = url.Host
	proxy.ServeHTTP(w, r)
}

func isInPath(basePath string, path string) bool {
	lenPath := len(path)
	lenBasePath := len(basePath)
	if path == basePath {
		return true
	} else if lenPath > lenBasePath && path[0:lenBasePath] == basePath {
		if path[lenBasePath] == '?' || path[lenBasePath] == '/' {
			return true
		}
	}
	return false
}

func handleRequestAndRedirect(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path
	dest := frontendURL
	if isInPath(apiPath, path) || isInPath(imagePath, path) {
		dest = backendURL
	}
	serveReverseProxy(dest, w, r)
}

func main() {
	config()
	printConfig()
	http.HandleFunc("/", handleRequestAndRedirect)
	if err := http.ListenAndServe(":"+port, nil); err != nil {
		panic(err)
	}
}
