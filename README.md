# Reverse proxy server for Run for your life project!

## Prerequisite

* Go V1.10 or newer

## Build

```sh
go build -o main .
```

## Run
```sh
./main
```